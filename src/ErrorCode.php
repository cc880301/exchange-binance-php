<?php
namespace cc880301\Binance;

class ErrorCode {
    public static $MAP = [
        -4068 => ['msg' => '如果有仓位, 无法修改仓位方向。'],
        -4067 => ['msg' => '如果有挂单，无法修改仓位方向。'],
        -4046 => ['msg' => '不需要切换仓位模式'],
        -4061 => ['msg' => '订单的持仓方向和用户设置不一致。'],
        -4051 => ['msg' => '逐仓余额不足。'],
    ];
    public static $MAPS = [
        -1101 => [
            'Too many parameters sent for this endpoint.' => ['msg' => '为此端点发送的参数太多。']
        ]
    ];
}
